jQuery.sap.require("sap.ui.qunit.qunit-css");
jQuery.sap.require("sap.ui.thirdparty.qunit");
jQuery.sap.require("sap.ui.qunit.qunit-junit");
QUnit.config.autostart = false;

sap.ui.require([
	"sap/ui/test/Opa5",
	"freelance/com/vn/test/integration/pages/Common",
	"sap/ui/test/opaQunit",
	"freelance/com/vn/test/integration/pages/App",
	"freelance/com/vn/test/integration/pages/Browser",
	"freelance/com/vn/test/integration/pages/Master",
	"freelance/com/vn/test/integration/pages/Detail",
	"freelance/com/vn/test/integration/pages/NotFound"
], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "freelance.com.vn.view."
	});

	sap.ui.require([
		"freelance/com/vn/test/integration/NavigationJourneyPhone",
		"freelance/com/vn/test/integration/NotFoundJourneyPhone",
		"freelance/com/vn/test/integration/BusyJourneyPhone"
	], function () {
		QUnit.start();
	});
});