jQuery.sap.require("sap.ui.qunit.qunit-css");
jQuery.sap.require("sap.ui.thirdparty.qunit");
jQuery.sap.require("sap.ui.qunit.qunit-junit");
QUnit.config.autostart = false;

// We cannot provide stable mock data out of the template.
// If you introduce mock data, by adding .json files in your webapp/localService/mockdata folder you have to provide the following minimum data:
// * At least 3 Customers in the list
// * All 3 Customers have at least one Orders

sap.ui.require([
	"sap/ui/test/Opa5",
	"freelance/com/vn/test/integration/pages/Common",
	"sap/ui/test/opaQunit",
	"freelance/com/vn/test/integration/pages/App",
	"freelance/com/vn/test/integration/pages/Browser",
	"freelance/com/vn/test/integration/pages/Master",
	"freelance/com/vn/test/integration/pages/Detail",
	"freelance/com/vn/test/integration/pages/NotFound"
], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "freelance.com.vn.view."
	});

	sap.ui.require([
		"freelance/com/vn/test/integration/MasterJourney",
		"freelance/com/vn/test/integration/NavigationJourney",
		"freelance/com/vn/test/integration/NotFoundJourney",
		"freelance/com/vn/test/integration/BusyJourney",
		"freelance/com/vn/test/integration/FLPIntegrationJourney"
	], function () {
		QUnit.start();
	});
});