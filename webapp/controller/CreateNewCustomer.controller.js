/*global location */
sap.ui.define([
	"freelance/com/vn/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"freelance/com/vn/model/formatter",
	"sap/m/MessageToast"
], function(BaseController, JSONModel, formatter, MessageToast) {
	"use strict";

	return BaseController.extend("freelance.com.vn.controller.CreateNewCustomer", {

		formatter: formatter,

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		onInit: function() {
			// this.getRouter().getRoute("createnewcust").attachPatternMatched(this._onObjectMatched, this);
		},

		/* =========================================================== */
		/* event handlers                                              */
		/* =========================================================== */
		_onObjectMatched: function(oEvent) {

		},
		onExtend: function(evt) {
			// this.getView().byId("grid1").setVisible(false);
			if (evt.getSource().getPressed()) {
				this.getView().byId("grid1").setVisible(true);
			} else {
				this.getView().byId("grid1").setVisible(false);
			}
		},
		onChangeType: function(evt) {
			var sKey = this.getView().byId("sl_type").getSelectedItem().getKey();
			if (sKey === "person") {
				this.getView().byId("sl_title").setVisible(true);
				this.getView().byId("vb_person").setVisible(true);
				this.getView().byId("in_company").setVisible(false);
				this.getView().byId("in_idtype").setValue("Z00001");

			} else {
				this.getView().byId("sl_title").setVisible(false);
				this.getView().byId("vb_person").setVisible(false);
				this.getView().byId("in_company").setVisible(true);
				this.getView().byId("in_idtype").setValue("Z00003");
			}
		},
		onNavBack: function() {
			var oHistory = sap.ui.core.routing.History.getInstance(),
				sPreviousHash = oHistory.getPreviousHash(),
				oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
			if (sPreviousHash !== undefined) {
				// The history contains a previous entry
				history.go(-1);
			} else {
				// Navigate back to FLP home
				oCrossAppNavigator.toExternal({
					target: {
						shellHash: "#Shell-home"
					}
				});
			
			}
		}

	});

});